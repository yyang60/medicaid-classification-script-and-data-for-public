'''
    Created on May, 2020
    Author: Yuan-Chi Yang
    Objective: collecting the streaming data from twitter API, using keyword filter for Medicaid, modified for release
    Note:
        The users need to create Twitter API application and provide the credentials as file, 'twitter-api-credentials.'
        The users also need to set up MongoDB and provide the host name, database name, and collection name for storage.
'''

import sys
from tweepy import OAuthHandler, API, Stream
from tweepy.streaming import StreamListener
from urllib3.exceptions import ProtocolError
from collections import defaultdict
from datetime import datetime
from pymongo import MongoClient
import re
import json
from http.client import IncompleteRead
from urllib3.exceptions import ProtocolError


class Listener(StreamListener):
    tweet = ''
    def __init__(self, mongo_database, collection, output_file=sys.stdout):
        super(Listener,self).__init__()
        self.output_file = output_file
        self.mongo_database = mongo_database
        self.collection = collection

    def on_status(self, status):
        tweet_data = defaultdict()
        tweet_data['id_str'] = status.id_str

        tweet_data['created_at'] = status._json['created_at']
        tweet_data['is_extended'] = 'true'
        tweet_data['coordinates'] = status._json['coordinates']
        tweet_data['place'] = status._json['place']
        try:
            tweet_data['display_text_range'] = status.display_text_range
        except:
            tweet_data['display_text_range'] = []
        tweet_data['user'] = status.user._json
        tweet = ''

        if hasattr(status,'retweeted_status'):
            try:
                tweet = status.retweeted_status.extended_tweet['full_text'].encode("utf-8")
            except:
                tweet = status.retweeted_status.text
        else:
            try:
                tweet = status.extended_tweet["full_text"].encode("utf-8")
            except AttributeError:
                tweet = status.text

        try:
            tweet_data['text'] = tweet.decode('utf-8')
        except AttributeError:
            tweet_data['text'] = tweet

        if not re.match('RT @', tweet_data['text']):
            # for inserting into a mongodb
            self.mongo_database[self.collection].insert_one(tweet_data)

            print('One instance created at', status.created_at)
            #print('tweet written to file.')

    def on_error(self, status_code):
        print(status_code)
        return False

if __name__ == "__main__":

    # Load medicaid keywords
    medicaid_keywords = []
    infile = open('./medicaid_selected_keywords')
    for line in infile:
        medicaid_keywords.append(str.strip(line))
    medicaid_keywords = list(set(medicaid_keywords))
    print(medicaid_keywords)

    #  credentials
    with open('./twitter-api-credentials', 'r') as file:
        credentials = file.read().split('\n')

    ACCESS_TOKEN = credentials[0]
    ACCESS_TOKEN_SECRET = credentials[1]
    CONSUMER_KEY = credentials[2]
    CONSUMER_SECRET = credentials[3]

    auth = OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
    api = API(auth, wait_on_rate_limit=True,
              wait_on_rate_limit_notify=True)

    #   MongoDB
    MONGO_HOST = 
    database_name = 
    collection = 
    client = MongoClient(MONGO_HOST)
    mongo_database = client[database_name]   
    

    listener = Listener(mongo_database=mongo_database, collection=collection)

    stream = Stream(auth=api.auth, listener=listener,tweet_mode='extended')

    while True:
        try:
            print('Start streaming.')
            stream.filter(track=medicaid_keywords,languages=['en'])
        except KeyboardInterrupt:
            print("Stopped.")
            break
        except IncompleteRead as e:
            print('http.client.IncompleteRead!')
            print(str(e))
            continue
        except ProtocolError as e:
            print('urllib3.exceptions.ProtocolError!')
            print(str(e))
            continue
        except:
            e = sys.exc_info()[0]
            print(str(e))
            continue
        finally:
            print('Done.')
            stream.disconnect()
            client.close()

